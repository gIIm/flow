/*
  ==============================================================================

  This is an automatically generated GUI class created by the Projucer!

  Be careful when adding custom code to these files, as only the code within
  the "//[xyz]" and "//[/xyz]" sections will be retained when the file is loaded
  and re-saved.

  Created with Projucer version: 5.4.1

  ------------------------------------------------------------------------------

  The Projucer is part of the JUCE library.
  Copyright (c) 2017 - ROLI Ltd.

  ==============================================================================
*/

//[Headers] You can add your own extra header files here...
/*
==============================================================================

This file is part of JUCE examples from https://github.com/harry-g.
Copyright (c) 2017 Harry G.

Permission is granted to use this software under the terms of the GPL v2 (or any later version)

Details of these licenses can be found at: www.gnu.org/licenses

JUCE examples are distributed in the hope that they will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

==============================================================================
*/
//[/Headers]

#include "AudioPlayerEditor.h"


//[MiscUserDefs] You can add your own user definitions and misc code here...
#include "ThumbnailComp.h"
/**
    A simple container class for the UI of each audio file including
    - a file open button
    - the filename (including path)
    - an audio thumbnail
    - a mute button
*/
class AudioPlayerEditor::AudioFileUiBundle
{
public:
	AudioFileUiBundle(ImageButton *openFileButton, Label *filename, ThumbnailComp *thumbnail, Button *muteButton)
		: openFileButton(openFileButton), filename(filename), thumbnail(thumbnail), muteButton(muteButton) {};

	~AudioFileUiBundle() {};

	ScopedPointer<ImageButton> openFileButton;
	ScopedPointer<Label> filename;
	ScopedPointer<ThumbnailComp> thumbnail;
	ScopedPointer<Button> muteButton;
};
//[/MiscUserDefs]

//==============================================================================
AudioPlayerEditor::AudioPlayerEditor (AudioPlayerPlugin &processor)
    : AudioProcessorEditor(&processor), processor(processor)
{
    //[Constructor_pre] You can add your own custom stuff here..
    //[/Constructor_pre]

    back.reset (new ImageButton ("back"));
    addAndMakeVisible (back.get());
    back->setExplicitFocusOrder (1);
    back->setButtonText (String());
    back->addListener (this);

    back->setImages (false, true, true,
                     ImageCache::getFromMemory (back_png, back_pngSize), 1.0f, Colour (0x00000000),
                     Image(), 1.0f, Colour (0x00000000),
                     Image(), 1.0f, Colour (0x00000000));
    back->setBounds (8, 8, 40, 24);

    playPause.reset (new ImageButton ("playPause"));
    addAndMakeVisible (playPause.get());
    playPause->setExplicitFocusOrder (2);
    playPause->setButtonText (String());
    playPause->addListener (this);

    playPause->setImages (false, true, true,
                          ImageCache::getFromMemory (play_png, play_pngSize), 1.0f, Colour (0x00000000),
                          Image(), 1.0f, Colour (0x00000000),
                          Image(), 1.0f, Colour (0x00000000));
    playPause->setBounds (96, 8, 40, 24);

    stop.reset (new ImageButton ("stop"));
    addAndMakeVisible (stop.get());
    stop->setExplicitFocusOrder (3);
    stop->setButtonText (String());
    stop->addListener (this);

    stop->setImages (false, true, true,
                     ImageCache::getFromMemory (stop_png, stop_pngSize), 1.0f, Colour (0x00000000),
                     Image(), 1.0f, Colour (0x00000000),
                     Image(), 1.0f, Colour (0x00000000));
    stop->setBounds (56, 8, 40, 24);

    timeSlider.reset (new Slider ("timeSlider"));
    addAndMakeVisible (timeSlider.get());
    timeSlider->setExplicitFocusOrder (4);
    timeSlider->setRange (0, 100, 0);
    timeSlider->setSliderStyle (Slider::LinearHorizontal);
    timeSlider->setTextBoxStyle (Slider::NoTextBox, true, 55, 20);
    timeSlider->setColour (Slider::backgroundColourId, Colour (0xff242424));
    timeSlider->setColour (Slider::thumbColourId, Colours::red);
    timeSlider->setColour (Slider::trackColourId, Colour (0xff242424));
    timeSlider->setColour (Slider::rotarySliderFillColourId, Colour (0xff242424));
    timeSlider->setColour (Slider::rotarySliderOutlineColourId, Colour (0xff242424));
    timeSlider->setColour (Slider::textBoxHighlightColourId, Colour (0x66ffffff));
    timeSlider->addListener (this);

    timeSlider->setBounds (8, 40, 360, 22);

    timeLabel.reset (new Label ("timeLabel",
                                String()));
    addAndMakeVisible (timeLabel.get());
    timeLabel->setFont (Font ("Share Tech Mono", 36.0f, Font::plain).withTypefaceStyle ("Regular"));
    timeLabel->setJustificationType (Justification::centred);
    timeLabel->setEditable (false, false, false);
    timeLabel->setColour (TextEditor::textColourId, Colours::black);
    timeLabel->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    timeLabel->setBounds (380, 10, 180, 54);

    record.reset (new ImageButton ("record"));
    addAndMakeVisible (record.get());
    record->setButtonText (String());
    record->addListener (this);

    record->setImages (false, true, true,
                       ImageCache::getFromMemory (reclightred_png, reclightred_pngSize), 1.0f, Colour (0x00000000),
                       Image(), 1.0f, Colour (0x00000000),
                       Image(), 1.0f, Colour (0x00000000));
    record->setBounds (144, 8, 40, 24);

    zoomSlider.reset (new Slider ("zoom"));
    addAndMakeVisible (zoomSlider.get());
    zoomSlider->setTooltip (TRANS("zoom"));
    zoomSlider->setRange (0, 1, 0);
    zoomSlider->setSliderStyle (Slider::RotaryHorizontalVerticalDrag);
    zoomSlider->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
    zoomSlider->setColour (Slider::backgroundColourId, Colour (0xff242424));
    zoomSlider->setColour (Slider::thumbColourId, Colour (0xff929292));
    zoomSlider->setColour (Slider::trackColourId, Colour (0xff363636));
    zoomSlider->setColour (Slider::rotarySliderFillColourId, Colour (0xff363636));
    zoomSlider->setColour (Slider::rotarySliderOutlineColourId, Colour (0xff242424));
    zoomSlider->addListener (this);

    zoomSlider->setBounds (312, 0, 88, 40);

    zoomLabel.reset (new Label ("zoomLabel",
                                TRANS("Zoom")));
    addAndMakeVisible (zoomLabel.get());
    zoomLabel->setFont (Font ("Share Tech Mono", 16.0f, Font::plain).withTypefaceStyle ("Regular"));
    zoomLabel->setJustificationType (Justification::centredLeft);
    zoomLabel->setEditable (false, false, false);
    zoomLabel->setColour (TextEditor::textColourId, Colours::black);
    zoomLabel->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    zoomLabel->setBounds (296, 8, 53, 24);

    repeat.reset (new TextButton ("repeat"));
    addAndMakeVisible (repeat.get());
    repeat->addListener (this);
    repeat->setColour (TextButton::buttonColourId, Colour (0xff363636));
    repeat->setColour (TextButton::buttonOnColourId, Colour (0xff424242));
    repeat->setColour (TextButton::textColourOnId, Colours::grey);

    repeat->setBounds (200, 8, 60, 24);


    //[UserPreSize]
    addChildComponent(audioInputThumb = new LiveScrollingAudioDisplay());
    audioInputThumb->setVisible(false);
    //[/UserPreSize]

    setSize (600, 400);


    //[Constructor] You can add your own custom stuff here..
	//setResizable(true, true);
	//setResizeLimits(400, 400, Desktop::getInstance().getDisplays().getMainDisplay().userArea.getWidth(), Desktop::getInstance().getDisplays().getMainDisplay().userArea.getHeight());
	setSize(600, 400);

    repeat->setClickingTogglesState(true);

    formatManager.registerBasicFormats();
	updateUiLayout();
    updatePlayPauseButton(true);
	startTimer(UI_TIMER_MS);
    //[/Constructor]
}

AudioPlayerEditor::~AudioPlayerEditor()
{
    //[Destructor_pre]. You can add your own custom destruction code here..
    stopTimer();
    while(isTimerRunning());

    //[/Destructor_pre]

    back = nullptr;
    playPause = nullptr;
    stop = nullptr;
    timeSlider = nullptr;
    timeLabel = nullptr;
    record = nullptr;
    zoomSlider = nullptr;
    zoomLabel = nullptr;
    repeat = nullptr;


    //[Destructor]. You can add your own custom destruction code here..
    audioFiles.clear();
    if (processor.hasRecorder()) {
        processor.recorder->removeAudioCallback(audioInputThumb);
    }
    audioInputThumb = nullptr;

    processor.editorBeingDeleted(this);
    //[/Destructor]
}

//==============================================================================
void AudioPlayerEditor::paint (Graphics& g)
{
    //[UserPrePaint] Add your own custom painting code here..
    //[/UserPrePaint]

    g.fillAll (Colour (0xff5e5e5e));

    //[UserPaint] Add your own custom painting code here..
    //[/UserPaint]
}

void AudioPlayerEditor::resized()
{
    //[UserPreResize] Add your own custom resize code here..
    //[/UserPreResize]

    //[UserResized] Add your own custom resize handling here..
	Rectangle<int> r = getFileArea();
    int i = 0;
    // go through all file UI and resize them
    for (const auto& file : audioFiles) {
		file->openFileButton->setBounds(r.withTrimmedTop(10).withSize(40, 24));
		file->filename->setBounds(r.withTrimmedTop(10).withTrimmedLeft(40).withSize(160, 24));
		file->thumbnail->setBounds(r.withLeft(220).withHeight(getFileLineHeight() - 10));
        file->muteButton->setBounds(r.withTrimmedTop(40).withTrimmedLeft(40).withSize(60, 24));

        // index == number of playback files -> this must be the recorder, it additionally has a LiveScrollingAudioDisplay
        if (i == getNumFiles(true)) {
            // set input thumb size same as file thumbnail
            audioInputThumb->setBounds(r.withLeft(260).withHeight(getFileLineHeight() - 10));
        }
        i++;

        r.removeFromTop(getFileLineHeight());
	}

	repeat->setBounds(200, 10, 60, 24);
	zoomLabel->setBounds(280, 10, 40, 24);
	zoomSlider->setBounds(300, 5, 88, 40);
	timeLabel->setBounds(380, 10, 180, 54);

    //[/UserResized]
}

void AudioPlayerEditor::buttonClicked (Button* buttonThatWasClicked)
{
    //[UserbuttonClicked_Pre]
    //[/UserbuttonClicked_Pre]

    if (buttonThatWasClicked == back.get())
    {
        //[UserButtonCode_back] -- add your button handler code here..
		processor.setReadPosition(0);
		updateTime(); // to update slider and label
		back->setEnabled(false);
        //[/UserButtonCode_back]
    }
    else if (buttonThatWasClicked == playPause.get())
    {
        //[UserButtonCode_playPause] -- add your button handler code here..
		updateLoopState(repeat->getToggleState());
		if ((getTransportState() == Stopped) || (getTransportState() == Paused))
			changeState(Starting);
		else if (isPlaying())
			changeState(Pausing);
        //[/UserButtonCode_playPause]
    }
    else if (buttonThatWasClicked == stop.get())
    {
        //[UserButtonCode_stop] -- add your button handler code here..
		changeState(Stopping);
        //[/UserButtonCode_stop]
    }
    else if (buttonThatWasClicked == record.get())
    {
        //[UserButtonCode_record] -- add your button handler code here..

        // check if a filename is selected
        if (processor.getFilename(getNumFiles(true))->isEmpty()) {
            AlertWindow::showMessageBoxAsync(AlertWindow::WarningIcon,
                "Cannot Record",
                "Please chose a filename to record first!");
        }
        // handle button press
		else if (!toggleRecordState()) {
			AlertWindow::showMessageBoxAsync(AlertWindow::WarningIcon,
				"Recording Error",
				"There was an errror trying to Record, e.g. file could not be opened for writing!");
		}
        //[/UserButtonCode_record]
    }
    else if (buttonThatWasClicked == repeat.get())
    {
        //[UserButtonCode_repeat] -- add your button handler code here..
        updateLoopState(repeat->getToggleState());
        //[/UserButtonCode_repeat]
    }

    //[UserbuttonClicked_Post]
	else {
		int fileNo = 0;
        // go through all file UIs
		for (const auto& file : audioFiles) {
			if (buttonThatWasClicked == file->muteButton) {
				processor.setMute(file->muteButton->getToggleState(), fileNo);
			}
			if (buttonThatWasClicked == file->openFileButton)
			{
                // fileno > max is the recording file
                bool isRecorder = fileNo == getNumFiles(true);

				String formats = formatManager.getWildcardForAllFormats();
				// MP3 not recommended, positioning not reliable
                formats = formats.replace("*.mp3;", "", true);

				// chose a file; recording file -> display different text and use save dialog
				FileChooser chooser(String::formatted(isRecorder ? "Select file to record" : "Select file %d to play...", fileNo+1),
					File::nonexistent, formats);
				if ( (!isRecorder && chooser.browseForFileToOpen()) || (isRecorder && chooser.browseForFileToSave(true)) )
				{
					// if Player is active, stop it
					if (isPlaying()) {
						changeState(Stopping);
						while (getTransportState() != Stopped);
					}
                    // no playing possible during file load
					changeState(NoFile);

                    // load the file in the procesor
					if (!processor.openFile(chooser.getResult(), fileNo)) {
						AlertWindow::showMessageBoxAsync(AlertWindow::WarningIcon,
							"File open Error",
							"File could not be opened! Probably the file is corrupt.");
                    }

                    // update filename & thumbnail, also after error (sets it empty)
                    updateFilename(fileNo);
                    updateThumbnail(fileNo);
                    // reset mute state and enable button
                    file->muteButton->setToggleState(false, dontSendNotification);
                    file->muteButton->setEnabled(true);

                    updateLoopState(repeat->getToggleState());

                    // any file loaded allows playing again
                    if (processor.isReadyToPlay())
                        changeState(Stopped);
                }
			}
			fileNo++;
		}
	}
    //[/UserbuttonClicked_Post]
}

void AudioPlayerEditor::sliderValueChanged (Slider* sliderThatWasMoved)
{
    //[UsersliderValueChanged_Pre]
    //[/UsersliderValueChanged_Pre]

    if (sliderThatWasMoved == timeSlider.get())
    {
        //[UserSliderCode_timeSlider] -- add your slider handling code here..
		int64 pos = (int64)(int(timeSlider->getValue() / 100 * getTotalLength())-1);
		setReadPosition(pos);
        //[/UserSliderCode_timeSlider]
    }
    else if (sliderThatWasMoved == zoomSlider.get())
    {
        //[UserSliderCode_zoomSlider] -- add your slider handling code here..
		for (const auto &file: audioFiles)
			file->thumbnail->setZoomFactor(zoomSlider->getValue());
        //[/UserSliderCode_zoomSlider]
    }

    //[UsersliderValueChanged_Post]
    //[/UsersliderValueChanged_Post]
}



//[MiscUserCode] You can add your own definitions of your custom methods or any other code here...
/**
    Set position in seconds
    @param pos position in seconds
*/
void AudioPlayerEditor::setPosition(double pos) {
    processor.setPosition(pos);
    updateTime(); // to update slider and label
}

/**
    Set position in samples
    @param pos position in samples
*/
void AudioPlayerEditor::setReadPosition(int64 pos) {
    processor.setReadPosition(pos);
    updateTime(); // to update slider and label
}

/**
    Do not show the audio thumbnail of the file with given index
    @param fileNo index of the file
*/
void AudioPlayerEditor::hideThumbnail(int fileNo)
{
    if (fileNo < getNumFiles(!processor.hasRecorderFile())) {
        AudioFileUiBundle *file = audioFiles[fileNo];
        if (file) {
            file->thumbnail->setVisible(true);
            file->thumbnail->setFile("");
        }
    }
}

/**
    Update thumbnail of given file number by reloading the file and repainting
    @param fileNo index of the file
*/
void AudioPlayerEditor::updateThumbnail(int fileNo)
{
    if (fileNo < getNumFiles(!processor.hasRecorderFile())) {
        AudioFileUiBundle *file = audioFiles[fileNo];
        if (file) {
            file->thumbnail->setFile(*processor.getFullPath(fileNo));
            file->thumbnail->setVisible(true);
            file->thumbnail->repaint();
        }
    }
}

/**
    Repaint all thumbnails, e.g. after stopping.
*/
void AudioPlayerEditor::repaintThumbnails() {
    for (const auto& file : audioFiles)
        file->thumbnail->repaint();
}

/**
    Update filename label of given file number by setting the file name and repainting
    @param fileNo index of the file
*/
void AudioPlayerEditor::updateFilename(int fileNo)
{
    if (fileNo < getNumFiles()) {
        AudioFileUiBundle *file = audioFiles.getUnchecked(fileNo);
        file->filename->setText(*processor.getFilename(fileNo), dontSendNotification);
    }
}

/**
    Get number of files, optionally including a possible recorder.
    Same time, getNumFiles(true) is also equal to the index of the recorder file (if a recorder is there).
    @param      playbackOnly    if true, get number of playback files only;
                                default is false, recorder file will be counted
                                - this makes sense as you can also play it
    @returns    the number of files for playback, by default including the recorder file
    @see        AudioPlayerPlugin::getNumFiles
*/
int AudioPlayerEditor::getNumFiles(bool playbackOnly) {
    const ScopedLock lo(layoutLock);

	return processor.getNumFiles(playbackOnly);
}

/**
    Get drawing area for file UI

    @returns a rectangle of the area
*/
Rectangle<int> AudioPlayerEditor::getFileArea() {
	return Rectangle<int>(getLocalBounds().reduced(8).withTop(Y_OFFSET));
}

/**
    Get height for one line of file UI

    @returns height in pixels
*/
unsigned AudioPlayerEditor::getFileLineHeight() {
    return Y_LINE_HEIGHT;
}

/**
    Update the file UI.
    - Updates repeat/loop button status
    - Shows record button if a recorder is there
    - Reconstructs the entire UI if number of files was changed.
*/
void AudioPlayerEditor::updateUiLayout()
{
    bool hasRecorder = processor.hasRecorder();
    record->setVisible(true);

    // restore loop status
    if (processor.isLooping())
        repeat->setToggleState(true, dontSendNotification);

    int numFiles = getNumFiles();

    // number of files changed
    if (isVisible() && (numFiles != oldNoOfFiles || audioFiles.size() != numFiles)) {
        Logger::outputDebugString("Updating UI layout");
        const ScopedLock lo(layoutLock);

        fileUiInitialized = false;
        audioFiles.clear();

        setSize(getLocalBounds().getWidth(), (Y_OFFSET + numFiles * getFileLineHeight()));

        // recreate entire file UIs
		for (int i = 0; i < numFiles; i++) {
			// fileindex == number of playback files -> this must be the recorder
			bool recorder = (i == (numFiles - hasRecorder));

			ImageButton* openFile;
			addAndMakeVisible(openFile = new ImageButton("openFile"));
			openFile->setButtonText(String());
			openFile->addListener(this);
			openFile->setImages(false, true, true,
				ImageCache::getFromMemory(!recorder ? folder_png : record_png, !recorder ? folder_pngSize : record_pngSize), 1.000f, Colour(0x00000000),
				Image(), 1.000f, Colour(0x00000000),
				Image(), 1.000f, Colour(0x00000000));

			Label* filename;
			addAndMakeVisible(filename = new Label("filename", String()));
			filename->setFont(Font(15.00f, Font::plain));
			filename->setJustificationType(Justification::centredLeft);
			filename->setEditable(false, false, false);
			filename->setColour(TextEditor::textColourId, Colours::black);
			filename->setColour(TextEditor::backgroundColourId, Colour(0x00000000));

			ThumbnailComp* thumbnail;
			addAndMakeVisible(thumbnail = new ThumbnailComp(formatManager, *this, *zoomSlider.get()));
			thumbnail->setFile(*processor.getFullPath(i));

			TextButton* muteButton;
			addAndMakeVisible(muteButton = new TextButton("mute"));
            muteButton->setColour(TextButton::textColourOnId, Colours::grey);
			muteButton->addListener(this);
            muteButton->setClickingTogglesState(true);
            // restore mute status
            muteButton->setToggleState(processor.isMuted(i), dontSendNotification);

            // set file name
			std::unique_ptr<String>
				name = processor.getFilename(i);
            if (name->isEmpty()) {
				name->clear();
				name->append(NO_FILE,100);
				//mute button makes no sense
                muteButton->setEnabled(false);
            }
            filename->setText(*name, dontSendNotification);

			audioFiles.add(new AudioFileUiBundle(openFile, filename, thumbnail, muteButton));
		}
		fileUiInitialized = true;
		resized();
        repaint();
    }
	oldNoOfFiles = numFiles;
}

/**
    Set Play/Pause button to play or pause icon
    @param play true for play icon, false for pause icon
*/
void AudioPlayerEditor::updatePlayPauseButton(bool play)
{
	playPause->setImages(false, true, true,
		ImageCache::getFromMemory(play ? play_png : pause_png, play ? play_pngSize : pause_pngSize), 1.000f, Colour(0x00000000),
		Image(), 1.000f, Colour(0x00000000),
		Image(), 1.000f, Colour(0x00000000));
}

/**
    Checks if the recorder is currently recording.
    @returns false if there is no recorder
*/
bool AudioPlayerEditor::isRecording() {
    return (processor.hasRecorder() && processor.getRecordState() >= Stopped);
}

/**
    Updates the UI if status was changed between isRecording() <-> !isRecording().
    - updates the status image of the record button (pressed/not pressed)
    - updates the UI and processor for playback when recording was stopped
*/
void AudioPlayerEditor::updateRecordState()
{
	if (isRecording() != wasRecording) {
		wasRecording = isRecording();
        // update button image
		record->setImages(false, true, true,
			ImageCache::getFromMemory(isRecording() ? record_png : reclightred_png, isRecording() ? record_pngSize : reclightred_pngSize), 1.000f, Colour(0x00000000),
			Image(), 1.000f, Colour(0x00000000),
			Image(), 1.000f, Colour(0x00000000));

        // UI actions when recording was stopped
        if (!isRecording()) {
            recordingStopped();
        }
	}
}

/**
    Handling for record key pressed.
    Will prepare or stop recording.
    Recording is started with play button.
    Punch-In not yet supported.
    @returns true for success, false if there was an error (e.g. opening the file) or there is no recorder
*/
bool AudioPlayerEditor::toggleRecordState() {
    if (!processor.hasRecorder())
        return false;

    bool ret = true;

    // do nothing when not at the beginning or currently playing or starting/recording and record key pressed
    // (punch-in/out is not suppoerted)
    if (getCurrentPosition() > 0 || getTransportState() >= Paused || processor.getRecordState() >= Starting)
        ;

    // file not opened and record key pressed
    else if (processor.getRecordState() == NoFile) {
        // prepare recording
        ret = prepareRecording();
        // roll back if not successful
        if (!ret)
            recordingStopped();
    }

    // stop and unload for all other cases
    else {
        ret = processor.changeRecordState(Unloading);
        recordingStopped();
    }

    return ret;
}

/**
    Prepare UI and recorder for recording.
    - switch to LiveScrollingAudioDisplay
    - prepare UI status
    - prepare file for writing
    @returns true for success, false if there was an error (e.g. opening the file) or there is no recorder
*/
bool AudioPlayerEditor::prepareRecording() {
    if (!processor.hasRecorder())
        return false;

    // hide file thumbnail, show audio input
    hideThumbnail(getNumFiles(true));
    audioInputThumb->setVisible(true);

    // turn repeat off, makes no sense for recording
    repeat->setToggleState(false, NotificationType::sendNotification);
    repeat->setEnabled(false);

    // keep mute status for later, unmute recording channel
    wasMuted = audioFiles[getNumFiles(true)]->muteButton->getToggleState();
    audioFiles[getNumFiles(true)]->muteButton->setToggleState(false, dontSendNotification);
    audioFiles[getNumFiles(true)]->muteButton->setEnabled(false);

    // unload recorder file from player to allow overwriting
    processor.unloadRecorderFile();

    // prepare for recording (open file)
    return processor.changeRecordState(Stopping);
}

/**
    UI and processor actions to do when recording was stopped.
    - load recording file for playback
    - show audio thumbnail
*/
void AudioPlayerEditor::recordingStopped() {
    // hide audio input
    audioInputThumb->setVisible(false);

    // reload file to allow playback and restore mute state & button state, show file thumbnail
    processor.reloadRecorderFile(wasMuted);
    if (audioFiles[getNumFiles(true)]) {
        audioFiles[getNumFiles(true)]->muteButton->setToggleState(wasMuted, dontSendNotification);
        audioFiles[getNumFiles(true)]->muteButton->setEnabled(true);
        updateThumbnail(getNumFiles(true));
    }
}

/**
    Set all files to loop/not loop mode
    @param shouldLoop true to turn on loop mode
*/
void AudioPlayerEditor::updateLoopState(bool shouldLoop)
{
	processor.setAllLooping(shouldLoop);
}

/**
    Calculate and display time in min./sec./frames, if it was changed.
    Move time slider.
*/
void AudioPlayerEditor::updateTime() {
	double pos = getCurrentPosition();

	if (pos != previousPos) {
		previousPos = pos;

		double len = getLengthInSeconds();

		// don't send notification, as this would trigger sliderValueChanged -> setNextReadPosition (kinda notification endless loop)
		timeSlider->setValue(pos / len * 100, dontSendNotification);

		// calculate and display time
		const RelativeTime rPos(pos);
		const int minutes = ((int)rPos.inMinutes()) % 60;
		const int seconds = ((int)rPos.inSeconds()) % 60;
		const int frames = (int)(rPos.inMilliseconds() % 1000 * 0.075); // audio frames=1/75sec
		const String time(String::formatted("%02d:%02d:%02d", minutes, seconds, frames));

		timeLabel->setText(time, dontSendNotification);

        if (pos > 0 && !isRecording())
            back->setEnabled(true);
        else
            back->setEnabled(false);
	}
}

/**
    Timer callback to update the UI if needed, mainly
    - time display
    - transport state changed
    - recorder added/removed
    - number of files changed
*/
void AudioPlayerEditor::timerCallback() {

	TransportState state = getTransportState();

	if (isPlaying())
		updateTime();

    // transport state changed
	if (state != oldState)
	{
        const ScopedLock lo(layoutLock);

        oldState = state;

        if (fileUiInitialized) {
            // update button enable status and play/pause bitmap according to transport state
            switch (state)
            {
            case NoFile:
                back->setEnabled(false);
                playPause->setEnabled(false);
                stop->setEnabled(false);
                repeat->setEnabled(false);
                timeSlider->setEnabled(false);
                break;
            case Stopped:
                updatePlayPauseButton(true);
                playPause->setEnabled(true);
                stop->setEnabled(false);
                back->setEnabled(false);
                repeat->setEnabled(true);
                timeSlider->setEnabled(true);
                updateTime();
                repaintThumbnails();
                break;
            case Playing:
                updatePlayPauseButton(false);
                stop->setEnabled(true);
                repeat->setEnabled(!isRecording());
                timeSlider->setEnabled(!isRecording());
                back->setEnabled(!isRecording());
                break;
            case Paused:
                updatePlayPauseButton(true);
                updateTime();
                break;
            default:
                break;
            }
        }
	}

	bool hasRecorder = processor.hasRecorder();

    // recorder was added or removed
	if (hasRecorder != hadRecorder) {
		hadRecorder = hasRecorder;
        if (hasRecorder) {
            processor.recorder->addAudioCallback(audioInputThumb);
            processor.reloadRecorderFile(false);
        }
        updateRecordState();
	}
    else if (hasRecorder)
		updateRecordState();

    // update UI layout if needed (checked inside method)
    updateUiLayout();
}

//[/MiscUserCode]


//==============================================================================
#if 0
/*  -- Projucer information section --

    This is where the Projucer stores the metadata that describe this GUI layout, so
    make changes in here at your peril!

BEGIN_JUCER_METADATA

<JUCER_COMPONENT documentType="Component" className="AudioPlayerEditor" componentName=""
                 parentClasses="public AudioProcessorEditor, private Timer" constructorParams="AudioPlayerPlugin &amp;processor"
                 variableInitialisers="AudioProcessorEditor(&amp;processor), processor(processor)"
                 snapPixels="8" snapActive="1" snapShown="1" overlayOpacity="0.33"
                 fixedSize="0" initialWidth="600" initialHeight="400">
  <BACKGROUND backgroundColour="ff5e5e5e"/>
  <IMAGEBUTTON name="back" id="6004726561a75f83" memberName="back" virtualName=""
               explicitFocusOrder="1" pos="8 8 40 24" buttonText="" connectedEdges="0"
               needsCallback="1" radioGroupId="0" keepProportions="1" resourceNormal="back_png"
               opacityNormal="1.0" colourNormal="0" resourceOver="" opacityOver="1.0"
               colourOver="0" resourceDown="" opacityDown="1.0" colourDown="0"/>
  <IMAGEBUTTON name="playPause" id="30084e5ef045db45" memberName="playPause"
               virtualName="" explicitFocusOrder="2" pos="96 8 40 24" buttonText=""
               connectedEdges="0" needsCallback="1" radioGroupId="0" keepProportions="1"
               resourceNormal="play_png" opacityNormal="1.0" colourNormal="0"
               resourceOver="" opacityOver="1.0" colourOver="0" resourceDown=""
               opacityDown="1.0" colourDown="0"/>
  <IMAGEBUTTON name="stop" id="254c53ebeb5bf50b" memberName="stop" virtualName=""
               explicitFocusOrder="3" pos="56 8 40 24" buttonText="" connectedEdges="0"
               needsCallback="1" radioGroupId="0" keepProportions="1" resourceNormal="stop_png"
               opacityNormal="1.0" colourNormal="0" resourceOver="" opacityOver="1.0"
               colourOver="0" resourceDown="" opacityDown="1.0" colourDown="0"/>
  <SLIDER name="timeSlider" id="9ea57500d69f6164" memberName="timeSlider"
          virtualName="" explicitFocusOrder="4" pos="8 40 360 22" bkgcol="ff242424"
          thumbcol="ffff0000" trackcol="ff242424" rotarysliderfill="ff242424"
          rotaryslideroutline="ff242424" textboxhighlight="66ffffff" min="0.0"
          max="100.0" int="0.0" style="LinearHorizontal" textBoxPos="NoTextBox"
          textBoxEditable="0" textBoxWidth="55" textBoxHeight="20" skewFactor="1.0"
          needsCallback="1"/>
  <LABEL name="timeLabel" id="906dcb6c6fd6022" memberName="timeLabel"
         virtualName="" explicitFocusOrder="0" pos="380 10 180 54" edTextCol="ff000000"
         edBkgCol="0" labelText="" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="Share Tech Mono" fontsize="36.0"
         kerning="0.0" bold="0" italic="0" justification="36"/>
  <IMAGEBUTTON name="record" id="65adc4db1b11afcd" memberName="record" virtualName=""
               explicitFocusOrder="0" pos="144 8 40 24" buttonText="" connectedEdges="0"
               needsCallback="1" radioGroupId="0" keepProportions="1" resourceNormal="reclightred_png"
               opacityNormal="1.0" colourNormal="0" resourceOver="" opacityOver="1.0"
               colourOver="0" resourceDown="" opacityDown="1.0" colourDown="0"/>
  <SLIDER name="zoom" id="158cc789a211c210" memberName="zoomSlider" virtualName=""
          explicitFocusOrder="0" pos="312 0 88 40" tooltip="zoom" bkgcol="ff242424"
          thumbcol="ff929292" trackcol="ff363636" rotarysliderfill="ff363636"
          rotaryslideroutline="ff242424" min="0.0" max="1.0" int="0.0"
          style="RotaryHorizontalVerticalDrag" textBoxPos="NoTextBox" textBoxEditable="1"
          textBoxWidth="80" textBoxHeight="20" skewFactor="1.0" needsCallback="1"/>
  <LABEL name="zoomLabel" id="8af66e196b5cf0ee" memberName="zoomLabel"
         virtualName="" explicitFocusOrder="0" pos="296 8 53 24" edTextCol="ff000000"
         edBkgCol="0" labelText="Zoom" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="Share Tech Mono" fontsize="16.0"
         kerning="0.0" bold="0" italic="0" justification="33"/>
  <TEXTBUTTON name="repeat" id="d0e28cd35acebf04" memberName="repeat" virtualName=""
              explicitFocusOrder="0" pos="200 8 60 24" bgColOff="ff363636"
              bgColOn="ff424242" textColOn="ff808080" buttonText="repeat" connectedEdges="0"
              needsCallback="1" radioGroupId="0"/>
</JUCER_COMPONENT>

END_JUCER_METADATA
*/
#endif

//==============================================================================
// Binary resources - be careful not to edit any of these sections!

// JUCER_RESOURCE: back_png, 192, "../../Resources/Icons/back.png"
static const unsigned char resource_AudioPlayerEditor_back_png[] = { 137,80,78,71,13,10,26,10,0,0,0,13,73,72,68,82,0,0,0,36,0,0,0,36,8,4,0,0,0,75,9,80,19,0,0,0,135,73,68,65,84,120,1,237,208,33,14,132,
64,12,70,225,7,236,193,230,80,235,183,138,108,130,231,4,56,20,14,201,177,112,69,85,53,48,73,83,67,50,239,247,95,210,210,106,189,161,31,127,18,250,162,40,51,29,247,21,4,161,212,25,101,121,132,4,69,145,
58,179,210,67,0,114,204,0,113,200,152,205,152,24,100,204,206,7,226,144,49,39,35,82,31,135,131,140,137,204,67,146,5,193,228,79,171,143,2,120,202,61,59,156,81,27,67,22,181,102,82,125,22,181,208,229,80,115,
8,114,212,200,123,107,181,46,112,129,155,38,95,206,206,225,0,0,0,0,73,69,78,68,174,66,96,130,0,0};

const char* AudioPlayerEditor::back_png = (const char*) resource_AudioPlayerEditor_back_png;
const int AudioPlayerEditor::back_pngSize = 192;

// JUCER_RESOURCE: pause_png, 114, "../../Resources/Icons/pause.png"
static const unsigned char resource_AudioPlayerEditor_pause_png[] = { 137,80,78,71,13,10,26,10,0,0,0,13,73,72,68,82,0,0,0,36,0,0,0,36,8,6,0,0,0,225,0,152,152,0,0,0,57,73,68,65,84,120,1,237,214,161,13,
0,48,12,3,193,142,222,205,219,5,204,172,160,220,75,97,6,7,115,36,41,119,211,213,251,162,151,175,223,3,1,1,1,45,1,1,1,1,1,1,1,77,61,249,146,244,1,218,206,251,17,131,19,168,116,0,0,0,0,73,69,78,68,174,66,
96,130,0,0};

const char* AudioPlayerEditor::pause_png = (const char*) resource_AudioPlayerEditor_pause_png;
const int AudioPlayerEditor::pause_pngSize = 114;

// JUCER_RESOURCE: play_png, 185, "../../Resources/Icons/play.png"
static const unsigned char resource_AudioPlayerEditor_play_png[] = { 137,80,78,71,13,10,26,10,0,0,0,13,73,72,68,82,0,0,0,36,0,0,0,36,8,4,0,0,0,75,9,80,19,0,0,0,128,73,68,65,84,120,1,237,148,65,21,128,
32,16,68,39,2,17,140,96,4,35,216,68,27,64,19,105,162,13,180,129,52,144,6,234,158,184,120,252,207,211,206,191,255,7,176,59,242,124,197,211,11,202,173,168,128,136,94,78,13,140,200,88,20,8,145,113,105,100,
68,198,170,142,17,217,185,38,70,100,236,234,25,145,17,21,8,81,27,10,64,212,134,2,16,101,66,84,152,171,37,226,177,15,226,251,171,102,98,32,55,98,69,42,179,180,153,168,145,194,20,91,82,248,183,252,61,158,
7,138,83,172,60,17,98,98,106,0,0,0,0,73,69,78,68,174,66,96,130,0,0};

const char* AudioPlayerEditor::play_png = (const char*) resource_AudioPlayerEditor_play_png;
const int AudioPlayerEditor::play_pngSize = 185;

// JUCER_RESOURCE: stop_png, 100, "../../Resources/Icons/stop.png"
static const unsigned char resource_AudioPlayerEditor_stop_png[] = { 137,80,78,71,13,10,26,10,0,0,0,13,73,72,68,82,0,0,0,36,0,0,0,36,8,6,0,0,0,225,0,152,152,0,0,0,43,73,68,65,84,120,1,237,206,177,13,0,
0,8,195,48,254,127,26,126,160,29,109,41,123,230,7,128,45,149,51,100,40,103,200,80,192,144,33,67,134,12,1,112,81,185,66,204,239,167,238,124,0,0,0,0,73,69,78,68,174,66,96,130,0,0};

const char* AudioPlayerEditor::stop_png = (const char*) resource_AudioPlayerEditor_stop_png;
const int AudioPlayerEditor::stop_pngSize = 100;

// JUCER_RESOURCE: folder_png, 169, "../../Resources/Icons/folder.png"
static const unsigned char resource_AudioPlayerEditor_folder_png[] = { 137,80,78,71,13,10,26,10,0,0,0,13,73,72,68,82,0,0,0,36,0,0,0,36,8,4,0,0,0,75,9,80,19,0,0,0,112,73,68,65,84,120,1,237,208,59,14,128,
32,16,69,209,235,158,173,21,10,163,46,212,4,42,87,160,99,40,137,223,33,52,68,238,235,207,36,67,173,232,44,14,57,205,160,108,64,110,214,162,106,65,242,80,27,146,48,135,165,201,1,133,205,185,160,53,23,36,
122,200,132,189,65,251,215,203,255,133,30,95,124,157,41,25,210,255,168,66,125,152,26,82,44,202,37,51,142,168,49,25,154,32,174,195,171,17,143,165,224,106,7,17,189,84,151,230,197,93,238,0,0,0,0,73,69,78,
68,174,66,96,130,0,0};

const char* AudioPlayerEditor::folder_png = (const char*) resource_AudioPlayerEditor_folder_png;
const int AudioPlayerEditor::folder_pngSize = 169;

// JUCER_RESOURCE: reclightred_png, 546, "../../Resources/Icons/rec-lightred.png"
static const unsigned char resource_AudioPlayerEditor_reclightred_png[] = { 137,80,78,71,13,10,26,10,0,0,0,13,73,72,68,82,0,0,0,36,0,0,0,36,8,6,0,0,0,225,0,152,152,0,0,1,233,73,68,65,84,88,133,237,214,
77,107,19,81,20,198,241,159,47,180,8,74,191,128,11,5,247,130,116,149,162,159,164,31,196,15,16,10,74,117,85,92,74,21,20,33,162,139,44,76,55,82,176,198,157,237,66,4,23,153,190,137,88,154,198,182,89,52,70,
237,117,209,25,177,45,55,54,201,196,110,242,192,217,204,153,57,231,127,239,61,151,121,24,106,168,161,78,73,161,67,20,41,148,40,191,99,235,43,237,22,161,73,72,216,171,178,85,162,124,151,155,157,106,228,
2,84,164,48,79,109,147,48,71,120,67,88,37,180,8,187,132,79,233,179,57,66,157,48,79,173,72,97,32,64,51,76,109,178,95,73,155,135,127,196,46,161,114,0,182,255,136,123,185,2,61,231,73,66,248,112,2,144,163,
241,145,176,76,120,193,211,92,128,102,120,176,70,88,239,1,38,139,117,194,26,225,33,211,125,1,21,41,212,217,95,236,3,38,139,197,244,248,178,153,234,9,232,53,181,74,14,48,89,84,14,6,61,233,9,104,138,137,
58,97,59,71,160,237,244,246,77,49,17,235,123,54,150,184,198,237,37,140,245,178,154,136,198,240,62,173,29,123,231,124,44,113,153,194,207,28,97,50,93,192,69,10,93,3,93,101,108,16,64,87,48,194,165,88,254,
76,44,209,74,111,231,104,206,64,223,211,166,163,145,222,209,25,130,118,206,48,39,81,244,200,190,241,171,201,185,232,222,246,168,77,140,240,35,150,143,238,208,50,187,95,114,134,129,21,212,104,118,13,244,
153,183,131,24,234,189,131,218,213,174,129,18,238,92,199,78,142,48,59,184,145,214,238,250,227,128,121,146,255,253,235,232,120,203,22,152,28,39,44,117,189,156,227,90,194,56,97,129,201,158,10,100,54,97,
150,233,188,236,199,108,191,246,35,139,60,12,218,75,158,253,93,179,47,160,224,176,133,109,156,0,164,65,120,149,122,160,199,220,63,90,175,111,160,224,143,201,79,234,14,155,252,118,26,171,142,153,252,100,
96,38,255,40,88,137,114,149,198,6,237,12,104,131,118,149,70,137,242,52,183,58,213,24,106,168,161,78,75,191,1,122,125,64,163,173,132,46,52,0,0,0,0,73,69,78,68,174,66,96,130,0,0};

const char* AudioPlayerEditor::reclightred_png = (const char*) resource_AudioPlayerEditor_reclightred_png;
const int AudioPlayerEditor::reclightred_pngSize = 546;

// JUCER_RESOURCE: record_png, 234, "../../Resources/Icons/record.png"
static const unsigned char resource_AudioPlayerEditor_record_png[] = { 137,80,78,71,13,10,26,10,0,0,0,13,73,72,68,82,0,0,0,36,0,0,0,36,8,6,0,0,0,225,0,152,152,0,0,0,177,73,68,65,84,120,1,237,214,65,17,
132,48,12,64,209,149,18,73,145,128,3,234,8,156,80,7,32,97,29,20,7,217,127,64,192,150,52,51,57,244,207,188,123,134,6,232,39,105,179,217,76,176,226,192,9,123,156,56,176,66,16,158,96,131,253,105,139,28,76,
209,96,157,26,20,67,91,96,78,75,192,48,110,10,87,130,6,27,164,121,119,106,135,13,182,123,158,142,5,17,116,87,96,65,10,186,171,176,32,21,221,125,97,65,78,116,103,193,18,15,148,224,200,174,252,75,157,255,
181,207,255,97,204,246,235,32,18,220,176,65,110,8,92,41,108,16,205,127,65,115,164,184,95,30,147,34,36,193,222,185,192,130,240,4,5,21,23,236,113,161,162,64,48,155,205,210,245,3,85,232,193,19,245,189,185,
15,0,0,0,0,73,69,78,68,174,66,96,130,0,0};

const char* AudioPlayerEditor::record_png = (const char*) resource_AudioPlayerEditor_record_png;
const int AudioPlayerEditor::record_pngSize = 234;


//[EndFile] You can add extra defines here...
//[/EndFile]
