/* =========================================================================================

   This is an auto-generated file: Any edits you make may be overwritten!

*/

#pragma once

namespace BinaryData
{
    extern const char*   back_png;
    const int            back_pngSize = 192;

    extern const char*   folder_png;
    const int            folder_pngSize = 169;

    extern const char*   JUCEAppIcon_png;
    const int            JUCEAppIcon_pngSize = 45854;

    extern const char*   pause_png;
    const int            pause_pngSize = 114;

    extern const char*   play_png;
    const int            play_pngSize = 185;

    extern const char*   reclightred_png;
    const int            reclightred_pngSize = 14831;

    extern const char*   record_png;
    const int            record_pngSize = 234;

    extern const char*   stop_png;
    const int            stop_pngSize = 100;

    extern const char*   average_mono_ttf;
    const int            average_mono_ttfSize = 1409208;

    extern const char*   gridnik_ttf;
    const int            gridnik_ttfSize = 36680;

    extern const char*   jura_regular_ttf;
    const int            jura_regular_ttfSize = 172232;

    extern const char*   new_alphabet_regular_ttf;
    const int            new_alphabet_regular_ttfSize = 8112;

    extern const char*   orbitron_regular_ttf;
    const int            orbitron_regular_ttfSize = 23584;

    extern const char*   share_tech_mono_regular_ttf;
    const int            share_tech_mono_regular_ttfSize = 42320;

    // Number of elements in the namedResourceList and originalFileNames arrays.
    const int namedResourceListSize = 14;

    // Points to the start of a list of resource names.
    extern const char* namedResourceList[];

    // Points to the start of a list of resource filenames.
    extern const char* originalFilenames[];

    // If you provide the name of one of the binary resource variables above, this function will
    // return the corresponding data and its size (or a null pointer if the name isn't found).
    const char* getNamedResource (const char* resourceNameUTF8, int& dataSizeInBytes);

    // If you provide the name of one of the binary resource variables above, this function will
    // return the corresponding original, non-mangled filename (or a null pointer if the name isn't found).
    const char* getNamedResourceOriginalFilename (const char* resourceNameUTF8);
}
